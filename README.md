`bamboo-gcov-plugin`
====================

# Introduction #

This is a python tool that converts `gcov` output files into a `clover.xml`
file so that `gcov` coverage statistics can be parsed by Atlassian Bamboo.

It provides a integration path for including `c++` coverage statistics in
Atlassian Bamboo.

# Documentation #

Atlassian host a
[documentation page](https://confluence.atlassian.com/x/ZYCyEQ) on their
Confluence Wiki.

The `gcov_to_clover.py` file is annotated with
[Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html) Python comments

# Usage #

The python file is executable and has full usage documentation.  Run the
following command to see the documentation:

`./gcov_to_clover -h`


# Contributing #

In case you would like to contribute, please sign Atlassian Contributor License Agreement:

* [Corporate CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [Individual CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)


Copyright @ 2013 - 2017 Atlassian Pty Ltd
Copyright @ 2013 Martin M Reed
Copyright @ 2012 Matt Clarkson
